<?php
namespace Fubber\Blade;

use eftec\bladeone\BladeOne;
use Fubber\ExceptionTemplate;
use Fubber\TemplateInterface;

class BladeTemplate implements TemplateInterface {

    protected readonly BladeOne $bladeOne;
    protected readonly string $name;
    protected readonly array $vars;

    public function __construct(BladeOne $bladeOne, string $name, array $vars) {
        $this->bladeOne = $bladeOne;
        $this->name = $name;
        $this->vars = $vars;
    }

    public function generate(): string {
        return $this->bladeOne->run($this->name, $this->vars);
    }

    public function __toString() {
        try {
            return $this->generate();
        } catch (\Throwable $e) {
            return (string) new ExceptionTemplate($e);
        }        
    }
}