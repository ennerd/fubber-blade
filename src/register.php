<?php

use Fubber\Blade\BladeTemplateEngine;
use Fubber\Kernel;
use Fubber\Hooks\Event;

/**
 * function(BladeOne $bladeEngine, BladeTemplateEngine $templateEngine)
 */
BladeTemplateEngine::$onBladeOneConstructed = new Event('BladeOne template engine constructed');

Kernel::$onConstructing->listen(function(Kernel $kernel) {
    $kernel->template->addEngine(new BladeTemplateEngine($kernel));
});

