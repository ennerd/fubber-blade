<?php
namespace Fubber\Blade;

use eftec\bladeone\BladeOne;
use Fubber\ExceptionTemplate;
use Fubber\Kernel;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Hooks\Event;
use Fubber\I18n\Translatable;
use Fubber\Microcache\MicrocacheInterface;
use Fubber\TemplateEngineInterface;
use Fubber\TemplateInterface;

class BladeTemplateEngine implements TemplateEngineInterface {

    public static Event $onBladeOneConstructed;

    protected BladeOne $bladeOne;

    public function __construct(protected readonly Kernel $kernel) {
        /*
        $this->bladeOne->errorCallBack = function(mixed ...$args) {
            var_dump($args);
            die("BladeTemplateEngine");
        };
        */
    }

    public function render(string $view, array $vars, string $pathHint=null): TemplateInterface {
        return new BladeTemplate($this->getBladeOne(), $view, $vars);
    }

    public function getExtensions(): array {
        return [ '.blade.php' ];
    }

    private function getBladeOne(): BladeOne {

        $benchConstruct = $this->kernel->instrumentation->benchmark('Constructing BladeOne template engine');

        $templatePaths = $this->kernel->views->all();
        $fileCacheRoot = $this->kernel->env->fileCacheRoot . \DIRECTORY_SEPARATOR . 'BladeOne';
        $instrumentation = $this->kernel->instrumentation;
        $microcache = $this->kernel->microcache->withNamespace(static::class);

        $instance = new class(
            $templatePaths,
            $fileCacheRoot,
            Kernel::debugMode() ? BladeOne::MODE_DEBUG : BladeOne::MODE_AUTO,
            $instrumentation,
            $microcache
        ) extends BladeOne {


            public function __construct(
                array $templatePaths,
                string $fileCacheRoot,
                int $mode,
                protected readonly Instrumentation $instrumentation,
                protected readonly MicrocacheInterface $microcache,
            ) {
                parent::__construct(
                    $templatePaths,
                    $fileCacheRoot,
                    $mode,
                );
                $this->setCompiledExtension('.bladec.php');
            }

            protected function runInternal($view, $variables = [], $forced = false, $isParent = true, $runFast = false): string {
                $perf = $this->instrumentation->benchmark("Running view `$view`");
                try {
                    return parent::runInternal($view, $variables, $forced, $isParent, $runFast);
                } finally {
                    $perf();
                }                
            }

            /**
             * Show an error in the web.
             *
             * @param string $id          Title of the error
             * @param string $text        Message of the error
             * @param bool   $critic      if true then the compilation is ended, otherwise it continues
             * @param bool   $alwaysThrow if true then it always throws a runtime exception.
             * @return string
             * @throws \RuntimeException
             */
            public function showError($id, $text, $critic = false, $alwaysThrow = false): string
            {
                \ob_get_clean();
                $e = Instrumentation::setExceptionInfo(
                    new BladeException("BladeOne Error [$id] $text"),
                    description: "Error parsing the template or error in application leaking to template rendering",
                    extraData: [
                        'Critical' => $critic ? "Yes" : "No, template generation continues"
                    ]
                );
                if ($this->throwOnError || $alwaysThrow || $critic === true) {
                    throw $e;
                }

                $msg = (string) new ExceptionTemplate($e);
                echo $msg;
                return $msg;
            }

            public function getTemplateFile($templateName = ''): string {
                if (\str_contains($templateName, '/')) {
                    $templateName = \strtr($templateName, ['/' => '.']);
                }
                return parent::getTemplateFile($templateName);
            }

            public function getCompiledFile($templateName = ''): string
            {
                $templateName = (empty($templateName)) ? $this->fileName : $templateName;
                $template = $this->getTemplateFile($templateName);
                $compiled = parent::getCompiledFile($templateName);
                Translatable::addPathAlias($compiled, $template);
                return $compiled;
            }
    
            /**
             * Find template file with the given name in all template paths in the order the paths were written
             *
             * @param string $name Filename of the template (without path)
             * @return string template file
             */
            protected function locateTemplate($name): string {
                /*
                if (!\str_ends_with($name, '.blade.php')) {
                    $name .= '.blade.php';
                }
                */
                $result = $this->microcache->fetch($name, function() use ($name) {
                    $notFoundPaths = [];
                    foreach ($this->templatePath as $dir) {
                        $path = $dir . '/' . $name;
                        if (\is_file($path)) {
                            return $path;
                        }
                        $notFoundPaths[] = $path;
                    }
                    $this->notFoundPath = implode(", ", $notFoundPaths);
                    return '';    
                }, 5);

                /*
                if ($result == '') {
                    return Kernel::getFrameworkPath() . '/assets/template-not-found.blade.php';
                }
                */

                return $result;
            }
            
        };

        self::$onBladeOneConstructed->trigger($instance, $this);

        $benchConstruct();

        return $instance;
    }

}