Fubber Framework Blade
======================

With the Fubber Framework Blade template engine, you can use Laravel style blade
templates in your web application.

To start using it simply require it and add the following to your Kernel

    \Fubber\Kernel::serve([

	...

        'templateFactory' => '\Fubber\Blade\Template::create',

	...

    ]);

You may also have to create a folder to store the compiled Blade templates.
