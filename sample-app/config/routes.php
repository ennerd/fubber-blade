<?php return [
    
    // Handled by a static method
	'GET /framework-components/blade/index.php' => function() {
	    return new \Fubber\View('index');
	},

];
